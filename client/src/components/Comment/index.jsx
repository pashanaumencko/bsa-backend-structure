import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const { comment: { id, body, createdAt, user }, updateComment, userId } = props;
    const [isEdit, setIsEdit] = useState(false);
    const [commentBody, setCommentBody] = useState(body);
    const date = moment(createdAt).fromNow();
    console.log(props.comment);
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                {isEdit
                    ? (
                        <Form
                            reply
                            onSubmit={() => {
                                updateComment(id, { body: commentBody });
                                setIsEdit(false);
                            }}
                        >
                            <Form.TextArea
                                defaultValue={body}
                                placeholder="Type a comment..."
                                onChange={event => setCommentBody(event.target.value)}
                                rows={4}
                            />
                            <Button type="submit" content="Update comment" labelPosition="left" icon="edit" positive />
                            <Button floated="right" onClick={() => setIsEdit(false)} color="blue">Cancel</Button>
                        </Form>
                    )
                    : (
                        <CommentUI.Text>
                            {body}
                        </CommentUI.Text>
                    )
                }
            </CommentUI.Content>
            <CommentUI.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn}>
                    <Icon name="thumbs up" />
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn}>
                    <Icon name="thumbs down" />
                </Label>
                {userId === user.id
                    ? (
                        <div>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEdit(true)}>
                                <Icon name="edit" />
                            </Label>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => updateComment(id, { body: commentBody, isActive: false })}>
                                <Icon name="trash" />
                            </Label>
                        </div>
                    )
                    : null
                }
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    userId: PropTypes.string.isRequired,
    updateComment: PropTypes.func.isRequired,
};

export default Comment;
