/* eslint-disable max-len */
/* eslint-disable no-trailing-spaces */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Form, Button } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({ post, userId, likePost, dislikePost, toggleExpandedPost, sharePost, updatePost }) => {
    const {
        id,
        image,
        imageId,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt
    } = post;
    const [isEdit, setIsEdit] = useState(false);
    const [postBody, setPostBody] = useState(body);
    const date = moment(createdAt).fromNow();
    // console.log(post);
    return (
        <Card style={{ width: '100%' }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                </Card.Meta>
                {isEdit
                    ? (
                        <Form 
                            reply
                            onSubmit={() => {
                                updatePost(id, { body: postBody, imageId });
                                setIsEdit(false);
                            }}
                        >
                            <Form.TextArea
                                defaultValue={body}
                                name="postUpdateInput"
                                placeholder="Type a post..."
                                onChange={event => setPostBody(event.target.value)}
                                rows={4}
                            />
                            <Button type="submit" content="Update post" labelPosition="left" icon="edit" positive />
                            <Button floated="right" onClick={() => setIsEdit(false)} color="blue">Cancel</Button>
                        </Form>
                    )
                    : (
                        <Card.Description>
                            {body}
                        </Card.Description>
                    )
                }
            </Card.Content>
            <Card.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
                {userId === user.id 
                    ? (
                        <div>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setIsEdit(true)}>
                                <Icon name="edit" />
                            </Label>
                            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => updatePost(id, { body: postBody, isActive: false, imageId })}>
                                <Icon name="trash" />
                            </Label>
                        </div>
                    )
                    : null
                }
               
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    userId: PropTypes.string.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
};

export default Post;
