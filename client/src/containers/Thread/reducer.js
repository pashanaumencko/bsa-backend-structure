/* eslint-disable no-case-declarations */
import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    UPDATE_POST,
    SET_EXPANDED_POST,
    SET_POST_REACTIONS,
    UPDATE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case SET_POST_REACTIONS:
            return {
                ...state,
                reactions: action.reactions,
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case UPDATE_POST:
            const filteredPosts = [...state.posts].filter(post => post.id !== action.id);
            console.log([...filteredPosts, action.post]);
            return {
                ...state,
                posts: [...filteredPosts, action.post]
            };
        case UPDATE_COMMENT:
            console.log(state.expandedPost);
            const expandedPostComments = [...state.expandedPost.comments];
            const filteredComments = expandedPostComments.filter(comment => comment.id !== action.id);
            // eslint-disable-next-line no-param-reassign
            state.expandedPost.comments = [...filteredComments, action.comment];
            return {
                ...state,
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        default:
            return state;
    }
};
