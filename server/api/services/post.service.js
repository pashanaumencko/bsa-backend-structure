import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const update = (postId, post) => postRepository.updateById(
    postId,
    post
);

export const getReactionForPost = () => postReactionRepository.getPostReactionsForPost();

export const setReaction = async (userId, { postId, isLike, isDislike }) => {
    // define the callback for future use as a promise
    let updateReaction;
    console.log(isLike, isDislike);
    if (isLike) {
        updateReaction = react => (react.isLike === isLike
            ? postReactionRepository.updateById(react.id, { isLike: false })
            : postReactionRepository.updateById(react.id, { isLike, isDislike: false }));
    } else {
        updateReaction = react => (react.isDislike === isDislike
            ? postReactionRepository.updateById(react.id, { isDislike: false })
            : postReactionRepository.updateById(react.id, { isDislike, isLike: false }));
    }

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    const result = reaction
        ? await updateReaction(reaction)
        : await postReactionRepository.create({ userId, postId, isLike, isDislike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
