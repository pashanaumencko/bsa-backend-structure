export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn('postReactions', 'isDislike', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }, { transaction }),
        ])),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn('postReaction', 'isDislike', { transaction }),
        ]))
};
