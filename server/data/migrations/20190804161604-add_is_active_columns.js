module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn('posts', 'isActive', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true
            }, { transaction }),
            queryInterface.addColumn('comments', 'isActive', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true
            }, { transaction }),
        ])),

    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn('posts', 'isActive', { transaction }),
            queryInterface.removeColumn('comments', 'isActive', { transaction }),
        ]))
};
